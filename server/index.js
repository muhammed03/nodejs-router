import { createServer } from 'http';

import uploadFileHandler from './routes/upload_files.js';
import getFileHandler from './routes/get_file.js'
import defaultRequestHandler from './routes/default-request.js';
import Router from './routes/router.js';

const { CLOUDY_PORT } = process.env

const router = Router()
  .post('/upload', uploadFileHandler)
  .post(defaultRequestHandler)
  .get(getFileHandler)

const server = createServer(router.serve);

server.listen(CLOUDY_PORT ?? 4000);

console.log(`Server started on port ${CLOUDY_PORT ?? 4000}`);
