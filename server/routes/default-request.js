const defaultRequestHandler = (_, res) => {
  res.statusCode = 400;
  res.end()
}

export default defaultRequestHandler;
