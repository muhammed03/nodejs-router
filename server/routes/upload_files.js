import {dirname, join} from "path";
import { createWriteStream } from "fs";
import {fileURLToPath} from "url";

const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);

const uploadFileHandler = (req, res) => {
  const path = req.headers["file-path"];
  if (!path) {
    res.statusCode = 400;
    res.end();
    return;
  }

  const outputPathFile = join(__dirname, '../../', 'files', path);
  console.log(outputPathFile)

  const writeStream = createWriteStream(outputPathFile);

  req.pipe(writeStream).on('finish', () => res.end());
}

export default uploadFileHandler;
