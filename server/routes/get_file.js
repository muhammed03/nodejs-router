import {dirname, join} from "path";
import {createReadStream} from "fs";
import {fileURLToPath} from "url";

const { CLOUDY_LOGIN, CLOUDY_PASS } = process.env

const unifiedCredentials = `${CLOUDY_LOGIN}:${CLOUDY_PASS}`;
const base64Credentials = Buffer.from(unifiedCredentials).toString('base64')
const expectedAuthorizationHeader = `Basic ${base64Credentials}`;

const __filename = fileURLToPath(import.meta.url);

const getFileHandler = (req, res) => {
  const filePath = join(__filename,  '../../../files', req.url);
  console.log(filePath)

  const readStream = createReadStream(filePath);
  res.setHeader('content-type', 'text/html');
  res.setHeader('content-encoding', 'gzip');
  res.setHeader('www-authenticate', "Basic");

  if (req.headers.authorization != expectedAuthorizationHeader) {
    res.statusCode = 401;
    res.end();
    return;
  }

  readStream.on('error', () => {
    res.statusCode = 404;
    res.end();
  }).pipe(res);
}

export default getFileHandler;
