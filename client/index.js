import { createReadStream, createWriteStream } from 'fs'
import { createGzip } from 'zlib';
import { request } from 'http';

const [ INPUT_PATH, OUTPUT_PATH, HOST ] = process.argv.slice(2);

const readStream = createReadStream(INPUT_PATH);
const writeStream = request(HOST, {
  method: 'POST',
  headers: {
    "file-path": OUTPUT_PATH
  }
});

const compressStream = createGzip()

readStream.on("end", () => {
  if (process.argv.includes("-v")) {
    console.log(`${INPUT_PATH} -> ${OUTPUT_PATH}`);
  }
})
  .pipe(compressStream)
  .pipe(writeStream)
  .on("error", (e) => {
    console.log(e)
    console.error(e.message);
    process.exit(1);
  })
